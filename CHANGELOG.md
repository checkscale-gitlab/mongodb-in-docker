# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.4.0](https://gitlab.com/LibreFoodPantry/spikeathons/lfp-spikeathon-summer-2020/mongodb-in-docker/compare/v1.3.0...v1.4.0) (2020-07-09)


### Features

* add summary of examples ([490a7a0](https://gitlab.com/LibreFoodPantry/spikeathons/lfp-spikeathon-summer-2020/mongodb-in-docker/commit/490a7a05487b4f3b7aea18860765cfb13e0ffe2d))

## [1.3.0](https://gitlab.com/LibreFoodPantry/spikeathons/lfp-spikeathon-summer-2020/mongodb-in-docker/compare/v1.2.0...v1.3.0) (2020-07-09)


### Features

* add ex9 and ex10 ([79115ec](https://gitlab.com/LibreFoodPantry/spikeathons/lfp-spikeathon-summer-2020/mongodb-in-docker/commit/79115ec2f930ccdfe83b105ef3fe187cccdd1453))
* don't require CTRL+C ([fd809b5](https://gitlab.com/LibreFoodPantry/spikeathons/lfp-spikeathon-summer-2020/mongodb-in-docker/commit/fd809b5e0028b522d6f2e4f68a1a5c387d8fa160))

## [1.2.0](https://gitlab.com/LibreFoodPantry/spikeathons/lfp-spikeathon-summer-2020/mongodb-in-docker/compare/v1.1.0...v1.2.0) (2020-06-15)


### Features

* Add ex8-refactor ([0f35842](https://gitlab.com/LibreFoodPantry/spikeathons/lfp-spikeathon-summer-2020/mongodb-in-docker/commit/0f35842ef6b5399615ab4a020c33b96eed766a12))

## [1.1.0](https://gitlab.com/LibreFoodPantry/spikeathons/lfp-spikeathon-summer-2020/mongodb-in-docker/compare/v1.0.0...v1.1.0) (2020-06-15)


### Features

* Add ex6-security-notes ([c2980c5](https://gitlab.com/LibreFoodPantry/spikeathons/lfp-spikeathon-summer-2020/mongodb-in-docker/commit/c2980c518d9e62c96cc01d84e28f9f7ae5cba2af))
* Add ex7-add-create ([06f4b5a](https://gitlab.com/LibreFoodPantry/spikeathons/lfp-spikeathon-summer-2020/mongodb-in-docker/commit/06f4b5a38420a0bdb3548d6db639da2aa0b7d7e5))

## 1.0.0 (2020-06-14)


### ⚠ BREAKING CHANGES

* Almost nothing is the same.

### Features

* add ex4-add-root ([82f3925](https://gitlab.com/LibreFoodPantry/spikeathons/lfp-spikeathon-summer-2020/mongodb-in-docker/commit/82f39258494f77837e4978b39ac83ee1d3c3c91c))
* add ex5-add-non-root ([70a1a80](https://gitlab.com/LibreFoodPantry/spikeathons/lfp-spikeathon-summer-2020/mongodb-in-docker/commit/70a1a80c9fcfb01fa116c81481a659a1c7aaf881))


* complete rewrite of examples ([06e0bae](https://gitlab.com/LibreFoodPantry/spikeathons/lfp-spikeathon-summer-2020/mongodb-in-docker/commit/06e0baefd6ca7c0ba705e09b06596f95342161c0))
