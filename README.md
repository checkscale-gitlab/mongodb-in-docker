# MongoDB Server and Client Examples

This repository contains a sequence of examples demonstrating how to setup and manage a MongoDB server in Docker with some best practices.

## Prerequisites

Before running the examples in this project, you'll need the following installed.

- Bash (Bourne shell or Z-shell should work as well)
- Git
- Docker (and Docker-Compose which comes with Docker)

If you are on Windows, you can install Git and Bash at the same time by
installing Git for Windows. When starting any of the examples, when a terminal
is needed, start a git-bash shell.

If you are using MacOS, your terminal is either Bash or Zsh. To install Git,
we recommend first installing Homebrew, and then installing Git using Homebrew:
`brew install git`. You can also install Docker through Homebrew:
`brew cask install docker`.

Examples

- ex1-client-server: Uses Docker and Docker compose to run up a MongoDB server in one container and accesses it using a Mongo client in a separate container.
- ex2-add-wait: Demonstrates how to have a container automatically wait until another container it depends on is ready to receive requests.
- ex3-add-tls: Encrypts all communication between containers on a network.
- ex4-add-root: Starts the configuration of role-based access control by protecting access to the MongoDB server by adding a password protected root account.
- ex5-add-non-root: Finishes the configuration of role-based access control by adding a non-root account that only has access to specific databases within the server.
- ex6-security-notes: Not really an example, but a short article on security in MongoDB.
- ex7-add-create: Begins the exploration of CRUD operations in MongoDB.
- ex8-refactor: Reorganizes the system to improve its design and understandability.
- ex9-crud: Finishes the exploration of some CRUD operations in MongoDB.
- ex10-persistence: Demonstrates how to persist data across containers.

