/*
    Moved connection code into this file.
    Other scripts `load()` this file to gain access to
    functions defined in this file.
*/

function remove_whitespace_from_both_ends(string) {
    return string.replace(/(^\s+|\s+$)/g,'');
}

function connect() {
    let conn = new Mongo("server:27017");
    let db = conn.getDB("test");
    let password = cat("/secrets/test-owner-password.txt");
    password = remove_whitespace_from_both_ends(password);
    db.auth("test-owner", password);
    return db;
}
