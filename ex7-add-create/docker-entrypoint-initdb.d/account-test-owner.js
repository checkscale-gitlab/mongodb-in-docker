conn = new Mongo();
db = conn.getDB("test");

var password = cat("/secrets/test-owner-password.txt");
// strip leading and trailing whitespace
password = password.replace(/(^\s+|\s+$)/g,'');

db.createUser(
  {
    user: "test-owner",
    pwd: password,
    roles: [
      { role: "readWrite", db: "test" },
      { role: "read", db: "reporting" }
    ]
  }
)
