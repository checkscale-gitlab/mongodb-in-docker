# ex2-add-wait

In ex1-client-server, we had to start the server, wait for it to be ready,
then run the client. In this example, we use
[docker-compose-wait](https://github.com/ufoscout/docker-compose-wait)
to automate this process.

First, position terminal in the root of this example.

```bash
cd ex2-add-wait
```

Review docker-compose.yml, client.dockerfile, and wait-then-run.sh.

When you are ready, run the example as follows

```bash
docker-compose up --detach
```

Now examine the logs as follows.

```bash
docker-compose logs
```

Examine the output and notice how the client keeps checking the server on port 27017.
When it sees it's ready, it sends its request and quits. You should see lines
similar to ex1-client-server that indicate that the test was successful.

If you don't see the above, try running `docker-compose logs` again after several seconds to see if the client and server have settled down.

```bash
docker-compose down
```

This example is done. Let's return to the parent directory.

```bash
cd ..
```
